const fs     = require('fs');
const moment = require('moment');

const subStart = moment.utc('2019-01-01');
const subEnd   = subStart.clone().add(10, 'years').subtract(1, 'millisecond');
const subCost  = 59400;

let visits = hashify(
    fs.readFileSync('data/visits_mike.cardwell-grepular.com.txt')
        .toString()
        .split('\n')
        .filter(l => l.match(/^\d+-\d+-\d+$/))
);

const nowDate = moment().format('YYYY-MM-DD');
const subDays = ( subEnd.unix() - subStart.unix() ) / 86400;

let visitCount = 0;

console.log('     Date     Cumulative  Visits per   Predicted    Total cost  Predicted cost');
console.log('                visits      week      total visits  per visit     per visit');
sep();

for (let cur = subStart.clone(), curDays = 1; cur.isBefore(subEnd); cur.add(1, 'day'), ++curDays) {

    const curDate = cur.format('YYYY-MM-DD');

    if (visits[curDate]) ++visitCount;

    const visitFrequency        = curDays / visitCount;
    const predictedVisits       = subDays / visitFrequency;
    const totalCostPerVisit     = subCost / visitCount;
    const predictedCostPerVisit = subCost / predictedVisits;

    console.log(
        visits[curDate] ? '+' : ' ',
        curDate + ' ',
        String(visitCount).padStart(10,  ' ') + ' ',
        (visitFrequency === Infinity ? '-' : (7/visitFrequency).toFixed(1)).padStart(10, ' ') + ' ',
        String(Math.floor(predictedVisits)).padStart(12, ' ') + ' ',
        humanCost(totalCostPerVisit).padStart(10, ' ') + ' ',
        humanCost(predictedCostPerVisit).padStart(14, ' '),
    );

    if (curDate === nowDate) sep('Today');
}

function humanCost(pence) {
    if (pence === Infinity) return '-';
    return '£' + (pence/100).toFixed(2);
}

function hashify(list) {
    return list.reduce((o, when) => {
        if (o.hasOwnProperty(when)) throw new Error('You specified ${when} more than once');
        o[when] = true;
        return o;
    }, {});
}

function sep(title='') {
    let len = 79;
    if (title.length === 0) {
        console.log(new Array(len + 2).join('-'));
    } else {
        len -= title.length;
        len = Math.floor(len / 2);
        const dash1 = new Array(len).join('-');
        const dash2 = new Array(len + title.length % 2).join('-');
        console.log(`${dash1} ${title} ${dash2}`);
    }
}
