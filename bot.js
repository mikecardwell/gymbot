const fs = require('fs').promises;
const moment = require('moment');

const {
    MatrixClient,
    SimpleFsStorageProvider,
    RichReply,
} = require('matrix-bot-sdk');

const {
    matrixServerUrl,
    accessToken,
    authorisedUsers,
    aliases={},
    dataDir,
    stateFile,
} = require('./config.json');

const storage = new SimpleFsStorageProvider(`${dataDir}/${stateFile}`);
const client  = new MatrixClient(matrixServerUrl, accessToken, storage);

const gymHelp = [
    'Hello. To see this message again, type "help".',
    'To log that you went to the gym today just type "gym".',
    'For yesterday, type "gym yesterday".',
    'For a specific date: "gym 2019-03-23".',
    'For last wednesday: "gym wed" or "gym wednesday".',
    'To remove an accidentally logged date: "ungym 2019-03-23".',
    'To get a list of your latest N visits (defaults to 3): "latest N"',
    'To get your all time stats: "stats".',
    'To get stats for a period: "stats 3 months". (or days/weeks/years).',
    'To get stats for the year 2020: "stats 2020".',
    'To reset your stats to zero: "reset".',
    'To restore your stats from the last time you ran "reset": "restore".',
].join(' ');

(async () => {

    await client.start();

    client.on('room.invite', async (roomId, event) => {
        try {
            const user = aliases[event.sender] || event.sender;
            if (user === await client.getUserId()) return; // Ignore self
            if (!isAuthorised(user)) return;
            await client.joinRoom(roomId);

            const reply   = RichReply.createFor(roomId, event, gymHelp, gymHelp);
            reply.msgtype = 'm.notice';
            await client.sendMessage(roomId, reply);
        } catch(err) {
            console.error(err);
        }
    });

    client.on('room.event', async (roomId, { type, content={}, ...event }) => {
        try {
            const user = aliases[event.sender] || event.sender;
            if (user === await client.getUserId()) return; // Ignore self
            if (!isAuthorised(user)) return;
            if (type === 'm.room.member' && content.membership === 'leave') {
                await client.leaveRoom(roomId);
            }
        } catch(err) {
            console.error(err);
        }
    });

    client.on('room.message', async (roomId, event) => {
        try {
            const user = aliases[event.sender] || event.sender;
            if (user === await client.getUserId()) return; // Ignore self
            if (!event || !event.content || !event.content.body) return;
            if (event.content.msgtype !== 'm.text') return;

            const sendMessage = async body => {
                const reply     = RichReply.createFor(roomId, event, body, body);
                reply.msgtype   = 'm.notice';
                return await client.sendMessage(roomId, reply);
            };

            if (!isAuthorised(user)) {
                return await sendMessage('ACCESS DENIED YOU MOTHER');
            }

            const msgTime = moment(event.origin_server_ts);
            const msgBody = event.content.body.toLowerCase().replace(/\s{2,}/g, ' ').trim();

            if (msgBody.match(/^(gym )?help$/)) {
                return await sendMessage(gymHelp);
            }

            // Gym Stats
            {
                const m = msgBody.match(/^(?:gym )?stats((\s+[0-9]+)?(\s+(day|week|month|year)s?)?)?$/)
                if (m) {
                    let msg;
                    if (typeof m[1] === 'undefined' || m[1] === '') {
                        msg = await gymStats(user);
                    } else if (m[1].match(/^\s*[0-9]{4}\s*$/)) {
                        const startDate = moment().year(m[1]).startOf('year');
                        const endDate   = startDate.clone().endOf('year');
                        if (startDate.isAfter()) {
                            return await sendMessage("I can't predicate the future, wise guy");
                        }
                        msg = await gymStats(user, { startDate, endDate });
                    } else {
                        const num  = m[2] ? parseInt(m[2], 10) : 1;
                        const type = m[4] ? m[4] : 'day';

                        const days = moment().diff(moment().subtract(num, type), 'days');
                        msg = await gymStats(user, { days });
                    }
                    return await sendMessage(msg);
                }
            }

            // Latest
            {
                const m = msgBody.match(/^(?:gym )?(?:last|latest)( (.+))?$/);
                if (m) {
                    return await sendMessage(await latest(user, (m[1]||'').trim()));
                }
            }

            // Reset
            if (msgBody.match(/^(gym )?reset$/)) {
                return await sendMessage(await reset(user));
            }

            // Reset
            if (msgBody.match(/^(gym )?restore$/)) {
                return await sendMessage(await restore(user));
            }

            // Parse message body
            const m = msgBody.match(/^(un)?gym(:?med)?( (.+))?$/);
            if (!m) return;
            const type = m[1] ? 'remove' : 'add';
            const args = typeof m[3] === 'undefined' ? 'today' : m[3].trim();

            // Figure out how many days ago the requestor is talking about
            let daysAgo;
            try {
                daysAgo = getDaysAgo({ args, msgTime });
            } catch(err) {
                if (err) return sendMessage(err.toString());
            }

            const date = msgTime.clone().subtract(daysAgo, 'days').startOf('day').format('YYYY-MM-DD');
            const humanDate = msgTime.clone().subtract(daysAgo, 'days').startOf('day').format('dddd LL');

            const agoStr = daysAgo === 0 ? 'today'
                         : daysAgo === 1 ? 'yesterday'
                         : `${daysAgo} days ago`;

            if (type === 'add') {
                if (await addVisit({ date, user })) {
                    return await sendMessage(`Adding to your log that you went to the gym ${agoStr}, on ${humanDate}. ${await gymStats(user, { days: 7 * 8 })}`);
                } else {
                    return await sendMessage(`It already says in my book that you went to the gym ${agoStr}, on ${humanDate}`);
                }
            } else if (type === 'remove') {
                if (await removeVisit({ date, user })) {
                    return await sendMessage(`Removed your gym log for ${agoStr}, on ${humanDate}. ${await gymStats(user, { days: 7 * 8 })}`);
                } else {
                    return await sendMessage(`According to my book, you never went to the gym ${agoStr}, on ${humanDate}, in the first place...`);
                }
            }

        } catch(err) {
            console.error(err);
        }
    });

})();

function visitFile(user) {
    return `${dataDir}/visits` + user.split(':')
        .map(s => s.replace(/[^a-z0-9.]/g, '_'))
        .join('-') + '.txt';
}

async function getVisits(user) {
    try {
        await fs.mkdir(dataDir, { recursive: true });
        return (await fs.readFile(visitFile(user)))
            .toString()
            .split('\n')
            .filter(l => l.match(/^\d+-\d+-\d+$/))
            .sort();
    } catch(err) {
        return [];
    }
}

async function reset(user) {
    const visits = await getVisits(user);
    if (visits.length === 0) {
        return "Your number of visits was already zero. There's nothing to reset";
    }
    // Backup the old version
    await fs.rename(visitFile(user), visitFile(user) + '.reset');
    return 'Your number of visits has been reset to zero. You can type "restore" to get them back';
}

async function restore(user) {
    try {
        await fs.rename(visitFile(user) + '.reset', visitFile(user));
    } catch(err) {
        return 'There is nothing to restore';
    }
    return 'Your visit list has been restored';
}

async function latest(user, num) {
    num = parseInt(num,10);
    if (isNaN(num)) num = 3;
    const visits = (await getVisits(user)).reverse().slice(0, num);
    if (visits.length === 0) {
        return 'No visits found';
    }
    if (visits.length === 1) {
        return 'You last visited on ' + visits[0];
    }
    return 'Your last ' + visits.length + ' gym visits were on: ' + visits.map(v => moment(v).format('dddd LL')).join(', ');
}

async function gymStats(user, opt={}) {
    let visitList = await getVisits(user);

    let startDate, endDate;
    if (Object.hasOwnProperty.call(opt, 'days')) {
        endDate   = moment().endOf('day');
        startDate = endDate.clone().subtract(opt.days, 'days');
    } else if (opt.startDate && opt.endDate) {
        endDate   = opt.endDate;
        startDate = opt.startDate;
        if (endDate.isAfter()) endDate = moment().endOf('day');
    } else {
        startDate = visitList.length ? moment(visitList[0]) : moment().startOf('day');
        endDate   = moment().endOf('day');
    }
    visitList = visitList.filter(v => (
        v >= startDate.format('YYYY-MM-DD') && v <= endDate.format('YYYY-MM-DD')
    ));

    const totalVisits   = visitList.length;
    const daysLogging   = endDate.diff(startDate, 'days');
    const weeksLogging  = daysLogging / 7;
    const visitsPerWeek = weeksLogging === 0 ? 0 : (totalVisits / weeksLogging).toFixed(1).replace(/\.0$/,'');

    const timeLoggingStr = (
          daysLogging > 30 ? `${endDate.diff(startDate, 'months', true).toFixed(0)} months`
        : daysLogging > 13 ? `${weeksLogging.toFixed(1).replace(/\.0$/,'')} weeks`
        : `${daysLogging} days`
    ).replace(/^(1 (.+?))s?$/, '$1');

    return [
        `During this period of ${timeLoggingStr}, you visited the gym on ${totalVisits} occasion${totalVisits === 1 ? '' : 's'}, `,
        `averaging ${visitsPerWeek} visits per week.`,
    ].join('');
}

async function addVisit({ date, user }) {
    const visits = await getVisits(user);
    if (visits.includes(date)) return false;
    visits.push(date);
    await fs.writeFile(visitFile(user), `${visits.sort().join('\n')}\n`);
    return true;
}

async function removeVisit({ date, user }) {
    let visits = await getVisits(user);
    if (!visits.includes(date)) return false;
    visits = visits.filter(d => d !== date);
    await fs.writeFile(visitFile(user), `${visits.sort().join('\n')}\n`);
    return true;
}

function getDaysAgo({ args='today', msgTime }) {

    if (args === 'today')     return 0;
    if (args === 'yesterday') return 1;

    let m = args.match(/^[0-9]{4}[-_][0-9]{2}[-_][0-9]{2}$/);
    if (m) return msgTime.diff(moment(args), 'days');

    m = args.match(/^([0-9]{2})[-_]([0-9]{2})[-_]([0-9]{4})$/);
    if (m) return msgTime.diff(moment(`${m[3]}-${m[2]}-${m[1]}`), 'days');

    m = args.match(/^-?([0-9]+)$/);
    if (m) return parseInt(m[1], 10);

    const dotw = {
        sun: 0, sunday:    0,
        mon: 1, monday:    1,
        tue: 2, tuesday:   2,
        wed: 3, wednesday: 3,
        thu: 4, thursday:  4,
        fri: 5, friday:    5,
        sat: 6, saturday:  6,
    };

    if (Object.hasOwnProperty.call(dotw, args)) {
        const dayNum = dotw[args];

        let base = msgTime.clone();
        do {
            base.subtract(1, 'day');
        } while (base.day() !== dayNum);
        return msgTime.diff(base, 'days');
    }

    throw new Error('Unable to figure out which day you meant');
}

function isAuthorised(user) {
    if (authorisedUsers.includes(user)) return true;
    const m = user.toLowerCase().match(/^@.+:([-_a-z0-9.]+)$/);
    if (m && authorisedUsers.includes(m[1])) return true;
    return false;
}
